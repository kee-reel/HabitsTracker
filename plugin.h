#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "../../Interfaces/Utility/i_user_task_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_repeat_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_date_data_ext.h"
#include "../../Interfaces/Utility/i_habits_tracker_data_ext.h"
#include "../../Interfaces/Utility/i_habits_tracker.h"
#include "habitstracker.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.UserTasksCalendar" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	virtual ~Plugin() = default;

	// PluginBase interface
protected:
	void onReady() override;

private:
	ReferenceInstancePtr<IHabitsTrackerDataExtention> m_habitsTracker;
	ReferenceInstancePtr<IUserTaskDataExtention> m_userTasks;
	ReferenceInstancePtr<IUserTaskRepeatDataExtention> m_repeatUserTasks;
	ReferenceInstancePtr<IUserTaskDateDataExtention> m_dateUserTasks;
	HabitsTracker* m_impl;
};
